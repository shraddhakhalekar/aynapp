const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Define Schema
let imgSchema = new Schema({
  _id: mongoose.Schema.Types.ObjectId,
  avatar: {
    type: Array
  }
}, {
  collection: 'images'
})

module.exports = mongoose.model('Images', imgSchema)
