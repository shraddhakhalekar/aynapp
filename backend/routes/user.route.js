let express = require('express'),
  multer = require('multer'),
  mongoose = require('mongoose'),
  router = express.Router();

// Multer File upload settings
const DIR = './public/';

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, DIR);
  },
  filename: (req, file, cb) => {
    const fileName = file.originalname.toLowerCase().split(' ').join('-');
    cb(null, fileName)
  }
});

var upload = multer({
  storage: storage,
  fileFilter: (req, file, cb) => {
    if (file.mimetype == "image/png" || file.mimetype == "image/jpg" || file.mimetype == "image/jpeg") {
      cb(null, true);
    } else {
      cb(null, false);
      return cb(new Error('Only .png, .jpg and .jpeg format allowed!'));
    }
  }
});

// Images model
let Images = require('../models/Images');

router.post('/upload', upload.array('avatar', 6), (req, res, next) => {
  
  const data = req.body.avatar;
  const buffer = Buffer.from(data, 'utf8')
  console.log("buffer is   : ", buffer);
  const img = new Images({
    _id: new mongoose.Types.ObjectId(),
    avatar: buffer
  });
  img.save().then(result => {
    console.log(result);
    res.status(201).json({
      message: "Done upload!",
      imgCreated: {
        _id: result._id,
        avatar: buffer,
      }
    })
  }).catch(err => {
    console.log(err),
      res.status(500).json({
        error: err
      });
  })
})

module.exports = router;